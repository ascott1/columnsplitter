﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Diagnostics;
using System.Threading;

namespace ActivityTypeSplitter
{
    class Program
    {
        /// <summary>
        /// Variable definitions
        /// </summary>
        private static double currPos = 0;

        private static int currPart = 1;
        private static long elapsedMs = 0;
        
        private static Stopwatch watch = Stopwatch.StartNew();

        public static string fileName = "";
        public static string baseName = "";

        public static string headers = "";
        public static string[] headerList;

        public static Dictionary<string, StreamWriter> fileStreams = new Dictionary<string, StreamWriter>();

        /// <summary>
        /// Entrypoint for the application
        /// </summary>
        /// <param name="args">Contains arguments, mainly the file to process</param>
        static void Main(string[] args)
        {
            try
            {
                fileName = Path.GetFileName(args[0].ToString());
                baseName = fileName.Split('.')[0];

                // Open and read file passed in via argument parameters
                using (var reader = new StreamReader(args[0]))
                {
                    // Read the very first line of the header, this will be placed in every part file
                    headers = reader.ReadLine();

                    Console.WriteLine("Select header to split file by:");

                    // Fetch options
                    headerList = headers.Split(',');

                    // Log the options
                    for (var i = 0; i < headerList.Length; i++)
                    {
                        Console.WriteLine("[" + i + "] | " + headerList[i]);
                    }

                    // Read input
                    Console.Write("> ");
                    var input = Console.ReadLine();
                    int inputNum = -1; 

                    // Parse input selection
                    if(int.TryParse(input, out inputNum))
                    {
                        Console.WriteLine("Selected: " + headerList[inputNum]);
                    }

                    Console.Clear();

                    // Start processing and read until end of file
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var lineItems = line.Split(',');

                        StreamWriter stream;

                        // Create stream if it does not exist
                        if (!fileStreams.ContainsKey(lineItems[inputNum]))
                        {
                            stream = File.CreateText(baseName + "-" + lineItems[inputNum] + ".csv");
                            stream.WriteLine(headers);

                            fileStreams[lineItems[inputNum]] = stream;
                        }
                        else
                        {
                            stream = fileStreams[lineItems[inputNum]];
                        }

                        stream.WriteLine(line);

                        Console.Write("\rCurrent line: {0} | File: {1}", currPos, baseName + "-" + lineItems[inputNum] + ".csv");

                        currPos++;
                    }

                    foreach (var f in fileStreams)
                    {
                        // Close the file
                        f.Value.Close();

                        // Compress the output CSV file
                        FileInfo compressFile = new FileInfo(baseName + "-" + f.Key + ".csv");
                        using (MemoryStream compressedMemStream = new MemoryStream())
                        {
                            using (FileStream originalFileStream = compressFile.OpenRead())
                            using (GZipStream compressionStream = new GZipStream(compressedMemStream, CompressionMode.Compress, leaveOpen: true))
                            {
                                originalFileStream.CopyTo(compressionStream);
                                originalFileStream.Close();
                            }
                            compressedMemStream.Seek(0, SeekOrigin.Begin);

                            FileStream compressedFileStream = File.Create(compressFile.FullName + ".gz");
                            compressedMemStream.WriteTo(compressedFileStream);
                            compressedFileStream.Close();
                            compressedMemStream.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                 // This exeption will usually occur on no input file sent in from argument parameters
                 Console.WriteLine("Exception caught: {0}\nPress enter to exit.", ex.Message);
                 Console.ReadLine();
                 Environment.Exit(0);
            }
        }
    }
}

